---

- sysctl: name={{ item.name }} value={{ item.value }} state=present sysctl_set=yes state=present reload=yes
  with_items:
    - { name: 'vm.swappiness', value: 5 }
    - { name: 'net.ipv4.tcp_tw_reuse', value: 1 }
    - { name: 'net.ipv4.tcp_tw_recycle', value: 1 }
    - { name: 'net.ipv4.tcp_max_tw_buckets', value: 500000 }
    - { name: 'net.core.somaxconn', value: 32768 }
    - { name: 'net.ipv4.tcp_max_syn_backlog', value: 32768 }
    - { name: 'net.ipv4.tcp_synack_retries', value: 3 }
    - { name: 'net.ipv6.conf.all.disable_ipv6', value: 1 }
    - { name: 'net.ipv6.conf.default.disable_ipv6', value: 1 }
  ignore_errors: true

#- name: install Webmin repo
#  copy: src=webmin.yum.repo dest=/etc/yum.repos.d/webmin.repo mode=0700
#
- name: download virtualmin install script
  get_url: url=http://software.virtualmin.com/gpl/scripts/install.sh  dest=/root/install.sh

#- name: copy virtualmin install script
#  copy: src=install.sh  dest=/root/install.sh

- name: set TMPNOEXEC status
  command: mount  -o remount,exec /tmp
  ignore_errors: true

- name: virtualmin SetHostname
  hostname: name={{ hostname }}

- name: virtualmin add hostname to hosts
  lineinfile: dest=/etc/hosts regexp=^{{ ansible_default_ipv4.address }} line="{{ ansible_default_ipv4.address }} {{ hostname }}" create=yes

- name: virtualmin install (possibly ~15 mins)
  shell: sh /root/install.sh --yes --hostname {{ hostname }} chdir=/root creates=/root/virtualmin-install.log

- stat: path=/etc/my.cnf.marker
  register: mycnfmarker

- copy: remote_src=True src=/usr/share/mysql/my-small.cnf dest=/etc/my.cnf
  when: mycnfmarker.stat.exists == False and ansible_memtotal_mb <= 512

- copy: remote_src=True src=/usr/share/mysql/my-medium.cnf dest=/etc/my.cnf
  when: mycnfmarker.stat.exists == False and ansible_memtotal_mb > 512 and ansible_memtotal_mb <= 1024

- copy: remote_src=True src=/usr/share/mysql/my-large.cnf dest=/etc/my.cnf
  when: mycnfmarker.stat.exists == False and ansible_memtotal_mb > 1024 and ansible_memtotal_mb <= 2048

- copy: remote_src=True src=/usr/share/mysql/my-huge.cnf dest=/etc/my.cnf
  when: mycnfmarker.stat.exists == False and ansible_memtotal_mb > 2048

- file: path=/etc/my.cnf.marker state=touch

  # https://mariadb.com/blog/starting-mysql-low-memory-virtual-machines
- name: vmwm turn off performance_schema for low-memory env.
  ini_file: dest=/etc/my.cnf section=mysqld option=performance_schema value=off
  when: mycnfmarker.stat.exists == False and ansible_memtotal_mb <= 2048

- name: vmwm restart mysql (c6)
  service: name=mysql state=restarted enabled=yes
  when: ansible_distribution_major_version == "6"
  
- name: vmwm restart mysql (c7)
  service: name=mariadb state=restarted enabled=yes
  when: ansible_distribution_major_version == "7" 

- name: vmwm - tuning webmin mysql config (c6)
  lineinfile: dest=/etc/webmin/mysql/config regexp=^{{ item.param }}= line="{{ item.newline }}"  backup=no
  with_items:
    - { param: 'stop_cmd'   , newline: 'stop_cmd=/etc/rc.d/init.d/mysql stop'   }
    - { param: 'start_cmd'  , newline: 'start_cmd=/etc/rc.d/init.d/mysql start' }
    - { param: 'encoding'   , newline: 'encoding='}
    - { param: 'charset'    , newline: 'charset=utf8'}
    - { param: 'login'      , newline: 'login='}
  when: ansible_distribution_major_version == "6"

- name: vmwm - tuning webmin mysql config (c7)
  lineinfile: dest=/etc/webmin/mysql/config regexp=^{{ item.param }}= line="{{ item.newline }}"  backup=no
  with_items:
    - { param: 'stop_cmd'   , newline: 'stop_cmd=/bin/systemctl stop  mariadb.service'   }
    - { param: 'start_cmd'  , newline: 'start_cmd=/bin/systemctl start  mariadb.service' }
    - { param: 'encoding'   , newline: 'encoding='}
    - { param: 'charset'    , newline: 'charset=utf8'}
    - { param: 'login'      , newline: 'login='}
  when: ansible_distribution_major_version == "7"

- name: vmwm post-install tuning
  script: vmwmtuning creates=/root/install/admin_passwd

- name: vmwm mysql_secure_installation
  script: mysql_secure_installation creates=/root/install/mysql_passwd

- name: vmwm - tuning webmin postfix config
  lineinfile: dest=/etc/postfix/main.cf regexp=^{{ item.param }} line="{{ item.newline }}"  backup=no create=yes
  with_items:
    - { param: 'inet_protocols '    , newline: 'inet_protocols = ipv4'   }
    - { param: 'virtual_alias_maps ', newline: 'virtual_alias_maps = hash:/etc/postfix/virtual'   }
    - { param: 'sender_bcc_maps '   , newline: 'sender_bcc_maps = hash:/etc/postfix/bcc'   }
    - { param: 'mailbox_command '   , newline: 'mailbox_command = /usr/bin/procmail-wrapper -o -a $DOMAIN -d $LOGNAME' }

- name: vmwm - tuning virtual-server config
  lineinfile: dest=/etc/webmin/virtual-server/config regexp=^{{ item.param }}= line="{{ item.newline }}"  backup=no
  with_items:
    - { param: 'plugins'       , newline: 'plugins=virtualmin-htpasswd' }
    - { param: 'quotas'        , newline: 'quotas=0' }
    - { param: 'bind_dmarc'    , newline: 'bind_dmarc=1' }
    - { param: 'bind_dmarcp'   , newline: 'bind_dmarcp=reject' }
    - { param: 'php_vars'      , newline: 'php_vars=' }
    - { param: 'php_ini_5.6'   , newline: 'php_ini_5.6=/etc/opt/rh/rh-php56/php.ini' }
    - { param: 'mail_autoconfig' , newline: 'mail_autoconfig=1' }
    - { param: 'can_letsencrypt' , newline: 'can_letsencrypt=1' }
    - { param: 'theme_switch_authentic-theme' , newline: 'theme_switch_authentic-theme=1' }

- name: vmwm - tuning webmin config
  lineinfile: dest=/etc/webmin/config regexp=^{{ item.param }}= line="{{ item.newline }}"  backup=no
  with_items:
    - { param: 'theme'       , newline: 'theme=virtual-server-theme' }
    - { param: 'mobile_theme', newline: 'mobile_theme=virtual-server-mobile' }
    - { param: 'tempdir'     , newline: 'tempdir=/home/.webmin' }

- name: vmwm - tuning usermin config
  lineinfile: dest=/etc/usermin/config regexp=^{{ item.param }}= line="{{ item.newline }}"  backup=no
  with_items:
    - { param: 'theme'       , newline: 'theme=virtual-server-theme' }
    - { param: 'mobile_theme', newline: 'mobile_theme=virtual-server-mobile' }

- name: vmwm - tuning webmin miniserv.conf
  lineinfile: dest=/etc/webmin/miniserv.conf regexp=^{{ item.param }}= line="{{ item.newline }}"  backup=no
  with_items:
    - { param: 'preroot'       , newline: 'preroot=virtual-server-theme' }

- name: vmwm read virtual-server config
  command: cat /etc/webmin/virtual-server/config
  register: webminvirtualserverconfig

- name: vmwm wizard-cli.cgi
  command: /usr/libexec/webmin/virtual-server/wizard.cgi
  args:
    chdir: /usr/libexec/webmin/virtual-server
  environment:
    WEBMIN_CONFIG: /etc/webmin/
    QUERY_STRING: "{{ item }}&parse=Next"
  with_items:
    - "step=1&preload=1&lookup=0"
    - "step=2&clamd=0"
    - "step=3&spamd=1"
    - "step=4&mysql=1&postgres=0"
    - "step=5&mypass_def=1"
    - "step=6&mysize=huge"
    - "step=7&prins={{ hostname }}&prins_skip=1&secns="
    - "step=8&hashpass=0"
    - "step=9"
  when: webminvirtualserverconfig.stdout.find('wizard_run=1') == -1
  ignore_errors: true

#- name: vmwm start mailman
#  service: name=mailman state=started enabled=yes

- name: vmwm package-updates/save_sched.cgi
  command: /usr/libexec/webmin/package-updates/save_sched.cgi
  args:
    chdir: /usr/libexec/webmin/package-updates
  environment:
    WEBMIN_CONFIG: /etc/webmin/
    QUERY_STRING: "mode=updates&search=&sched_def=1&sched=d&email={{sysemail}}&action=1&save=Save"
  ignore_errors: true

- name: vmwm virtualmin post-install create default domain
  shell: >
    virtualmin create-domain
    --domain default.tld
    --passfile /root/install/default_passwd
    --desc "default.tld"
    --web
    --unix
    --dir
    --ssl
    --mysql
    creates=/home/default
#   --db stat
#   --mysql-pass stat

- name: vmwm - dovecot bind to ipv4 only
  lineinfile: dest=/etc/dovecot/dovecot.conf regexp=^listen line="listen = *" backup=no

- name: vmwm - setup hostname-based relay (amhost.net)
  lineinfile: dest=/etc/postfix/main.cf regexp=^relayhost line="relayhost = relay.exmail.net" backup=no create=yes
  when: hostname.find('amhost.net') > 1 and ( ansible_virtualization_type == "xen" or ansible_virtualization_type == "kvm")

- name: vmwm - setup hostname-based relay (polusweb.com)
  lineinfile: dest=/etc/postfix/main.cf regexp=^relayhost line="relayhost = relay.comail.net" backup=no create=yes
  when: hostname.find('polusweb.com') > 1 and ( ansible_virtualization_type == "xen" or ansible_virtualization_type == "kvm")
