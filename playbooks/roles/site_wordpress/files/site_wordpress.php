<?php
/** Customs 001 */
define( 'AUTOMATIC_UPDATER_DISABLED', false );
define( 'WP_AUTO_UPDATE_CORE', 'minor' );
define( 'WP_MEMORY_LIMIT', '64M' );
define( 'JIGOSHOP_LOG_DIR', dirname(ABSPATH).'/logs/' );
define( 'DISALLOW_FILE_EDIT', true );

/** Customs 002 */
add_filter( 'auto_update_plugin', '__return_true' );
add_filter( 'auto_update_theme', '__return_true' );
