import datetime
import ConfigParser

from sqlalchemy import create_engine
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import Column, Integer, String, Text, DateTime
from sqlalchemy.orm import sessionmaker

cfg = ConfigParser.RawConfigParser()
cfg.read('config.cfg')
SQLALCHEMY_DATABASE_URI = cfg.get('logging', 'SQLALCHEMY_DATABASE_URI')
Base = declarative_base()


class Logs(Base):
    __tablename__ = 'logs'
    log_id = Column(Integer, primary_key=True)
    log_type = Column(String(32), index=True)
    log_msg = Column(Text)
    log_crtd = Column(DateTime, default=datetime.datetime.now)
    log_initid = Column(Integer, default=0)

    def __repr__(self):
        return '<Logs %r>' % self.log_id


class Storage(object):
    def __init__(self):
        engine = create_engine(SQLALCHEMY_DATABASE_URI, pool_recycle=360, echo=True)
        session = sessionmaker(bind=engine)
        self.session = session()

    def flush_log_to_database(self, log_msg, has_errors=False, log_initid=0):
        """Save log_msg to database"""
        log_type = 'info'
        if has_errors:
            log_type = 'error'
        log = Logs(log_type=log_type, log_msg=log_msg, log_initid=log_initid)
        try:
            self.session.add(log)
            self.session.commit()
        except Exception as error:
            print('caught this error: ' + repr(error))
        print log_initid, log_type
        print "flush_log_to_database complete"
        return log.log_id
