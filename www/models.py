#!/usr/bin/python

from www import db

ROLE_USER = 0
ROLE_ADMIN = 1


class Nodes(db.Model):
    node_id = db.Column(db.Integer, primary_key=True)
    node_name = db.Column(db.String(32), index=True, unique=True)
    node_user = db.Column(db.String(32), index=True)
    node_passwd = db.Column(db.String(32), index=True)
    node_email = db.Column(db.String(32), index=True)
    node_ip = db.Column(db.String(15), index=True)
    node_note = db.Column(db.String(256), index=True)
    node_role = db.Column(db.SmallInteger, default=ROLE_USER)
    node_crtd = db.Column(db.DateTime, default=db.func.now())
    node_uptd = db.Column(db.DateTime, default=db.func.now(), onupdate=db.func.now())

    def __repr__(self):
        return '<Nodes %r>' % self.node_name


class Domains(db.Model):
    domain_id = db.Column(db.Integer, primary_key=True)
    domain_name = db.Column(db.String(32), unique=True)
    domain_nodeid = db.Column(db.Integer, db.ForeignKey('nodes.node_id'))
    domain_dbpass = db.Column(db.String(32))
    domain_dbname = db.Column(db.String(32))
    domain_dbuser = db.Column(db.String(32))
    domain_ip = db.Column(db.String(15))
    domain_user = db.Column(db.String(16))
    domain_passwd = db.Column(db.String(32))
    domain_email = db.Column(db.String(32))
    domain_note = db.Column(db.String(256), index=True)
    domain_hiden = db.Column(db.SmallInteger, default=0)
    domain_crtd = db.Column(db.DateTime, default=db.func.now())
    domain_uptd = db.Column(db.DateTime, default=db.func.now(), onupdate=db.func.now())
    domain_core = db.Column(db.String(32))

    def __repr__(self):
        return '<Domains %r>' % self.domain_name


class Logs(db.Model):
    log_id = db.Column(db.Integer, primary_key=True)
    log_type = db.Column(db.String(32), index=True)
    log_msg = db.Column(db.Text)
    log_crtd = db.Column(db.DateTime, default=db.func.now())
    log_initid = db.Column(db.Integer, default=0)

    def __repr__(self):
        return '<Logs %r>' % self.log_id
