# https://serversforhackers.com/running-ansible-2-programmatically
import os
from ansible.inventory import Inventory
from ansible.vars import VariableManager
from ansible.parsing.dataloader import DataLoader
from ansible.executor import playbook_executor
from ansible.utils.display import Display
from jinja2 import Environment, FileSystemLoader
from tempfile import NamedTemporaryFile
from multiprocessing import Process, Queue


class Options(object):
    """
    Options class to replace Ansible OptParser
    """

    def __init__(self, verbosity=None, inventory=None, listhosts=None, subset=None, module_paths=None,
                 extra_vars=None, forks=None, ask_vault_pass=None, vault_password_files=None,
                 new_vault_password_file=None, output_file=None, tags=None, skip_tags=None, one_line=None,
                 tree=None, ask_sudo_pass=None, ask_su_pass=None, sudo=None, sudo_user=None, become=None,
                 become_method=None, become_user=None, become_ask_pass=None, ask_pass=None,
                 private_key_file=None, remote_user=None, connection=None, timeout=None, ssh_common_args=None,
                 sftp_extra_args=None, scp_extra_args=None, ssh_extra_args=None, poll_interval=None,
                 seconds=None, check=None, syntax=None, diff=None, force_handlers=None, flush_cache=None,
                 listtasks=None, listtags=None, module_path=None):
        self.verbosity = verbosity
        self.inventory = inventory
        self.listhosts = listhosts
        self.subset = subset
        self.module_paths = module_paths
        self.extra_vars = extra_vars
        self.forks = forks
        self.ask_vault_pass = ask_vault_pass
        self.vault_password_files = vault_password_files
        self.new_vault_password_file = new_vault_password_file
        self.output_file = output_file
        self.tags = tags
        self.skip_tags = skip_tags
        self.one_line = one_line
        self.tree = tree
        self.ask_sudo_pass = ask_sudo_pass
        self.ask_su_pass = ask_su_pass
        self.sudo = sudo
        self.sudo_user = sudo_user
        self.become = become
        self.become_method = become_method
        self.become_user = become_user
        self.become_ask_pass = become_ask_pass
        self.ask_pass = ask_pass
        self.private_key_file = private_key_file
        self.remote_user = remote_user
        self.connection = connection
        self.timeout = timeout
        self.ssh_common_args = ssh_common_args
        self.sftp_extra_args = sftp_extra_args
        self.scp_extra_args = scp_extra_args
        self.ssh_extra_args = ssh_extra_args
        self.poll_interval = poll_interval
        self.seconds = seconds
        self.check = check
        self.syntax = syntax
        self.diff = diff
        self.force_handlers = force_handlers
        self.flush_cache = flush_cache
        self.listtasks = listtasks
        self.listtags = listtags
        self.module_path = module_path


class Runner(object):
    def __init__(self, playbook_path, run_data, verbosity=0):
        self.playbook_path = playbook_path
        self.run_data = run_data

        self.options = Options()
        self.options.verbosity = verbosity
        self.options.connection = 'ssh'
        self.options.tags = ''
        self.options.skip_tags = ''

        # Set global verbosity
        self.display = Display()
        self.display.verbosity = self.options.verbosity
        # Executor appears to have it's own 
        # verbosity object/setting as well
        playbook_executor.verbosity = self.options.verbosity

        # Gets data from YAML/JSON files
        self.loader = DataLoader()

        # All the variables from all the various places
        self.variable_manager = VariableManager()
        self.variable_manager.extra_vars = self.run_data

        # Set inventory, using most of above objects
        self.inventory_file = self.get_inventory_file()
        self.inventory = Inventory(loader=self.loader, variable_manager=self.variable_manager,
                                   host_list=self.inventory_file)
        self.variable_manager.set_inventory(self.inventory)

        # Setup playbook executor, but don't run until run() called
        self.pbex = playbook_executor.PlaybookExecutor(
            playbooks=[playbook_path],
            inventory=self.inventory,
            variable_manager=self.variable_manager,
            loader=self.loader,
            options=self.options,
            passwords={})

    def get_inventory_file(self):
        jsonargs = self.run_data
        jinja2_env = Environment(
            loader=FileSystemLoader(os.path.join(os.path.dirname(__file__), 'templates')))
        inventory_template = jinja2_env.get_template('hosts.j2')
        rendered_inventory = inventory_template.render(jsonargs)
        hosts = NamedTemporaryFile(delete=False)
        print hosts.name
        hosts.write(rendered_inventory)
        hosts.close()
        return hosts.name

    def run(self):
        self.pbex.run()
        stats = self.pbex._tqm._stats
        has_errors = False
        hosts = sorted(stats.processed.keys())
        for h in hosts:
            t = stats.summarize(h)
            if t['unreachable'] > 0 or t['failures'] > 0:
                has_errors = True
        self.pbex._tqm.send_callback(
            'import_run_data',
            run_data=self.run_data,
            has_errors=has_errors
        )
        # os.remove(self.inventory_file)


def recipe(playbook_path, jsonargs):
    runner = Runner(
        playbook_path=playbook_path,
        run_data=jsonargs,
        verbosity=1
    )
    try:
        runner.run()
    except Exception as error:
        print('runner.run() caught this error: ' + repr(error))

def worker(queue):
    while True:
        playbook_path, jsonargs = queue.get()
        recipe(playbook_path, jsonargs)
        # BFR.p.join(1)
        # BFR.p.terminate()
        print "### kassa vil'na"

class BFR(object):
    queue = Queue()
    p = Process(target=worker, args=(queue,))
    def __init__(self):
        self.p.start()
        print "### threadstarted !!!!"

