#!/usr/bin/python
import random
import pycurl
#import validators
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from subprocess import Popen, PIPE


def pwgen(pw_length=12, alphabet="abcdefghijkmnopqrstuvwxyz3479ACEFHJKLMNPRTUVWXY"):
    mypw = ""
    for i in range(pw_length):
        next_index = random.randrange(len(alphabet))
        mypw += alphabet[next_index]
    return mypw


def curl_calback_url(base64_url):
    if base64_url:
        try:
            base64_url.decode('base64')
        except Exception as error:
            print('curl_calback_url(base64_url) caught this error: ' + repr(error))
        decoded_url = base64_url.decode('base64')
#         if validators.url(decoded_url):
#             print "curl_calback_url decoded."
#             print decoded_url
#             c = pycurl.Curl()
#             c.setopt(c.URL, decoded_url)
#             c.perform()


def send_mail(sysmail, txt, logid, has_errors=False):
    if has_errors:
        with_result = "failed"
    else:
        with_result = "successful"
    if len(sysmail) < 6:
        return None
    # sysmail = "default@mail.net" if sysmail == "" else sysmail
    print "###### %s ######" % sysmail
    mime_txt = MIMEText(txt, 'plain')
    msg = MIMEMultipart("alternative")
    msg["From"] = "maya"
    msg["To"] = sysmail
    msg["Subject"] = "bZzzz, setup # %s %s" % (logid, with_result)
    msg.attach(mime_txt)
    p = Popen(["/usr/sbin/sendmail", "-t"], stdin=PIPE)
    p.communicate(msg.as_string())
