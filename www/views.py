#!/usr/bin/python
# -*- coding: utf-8 -*-
# http://marshmallow.readthedocs.org/en/latest/examples.html

from flask import jsonify, make_response
from flask import render_template
from flask import request
from flask import send_from_directory
from sqlalchemy.exc import IntegrityError

from www import app
from www import db
from www.functions import pwgen
from www.pb_logger import Storage
from www.pb_runner import BFR
from .models import Nodes, Domains, Logs
from .schemas import NodesSchema, DomainsSchema
from .forms import AddNode, AddDomain

SECRET_KEY = app.config['SECRET_KEY']
UPLOAD_FOLDER = app.config['UPLOAD_FOLDER']
STATIC_FOLDER = app.config['STATIC_FOLDER']

BFR()


@app.errorhandler(400)
def not_found(error):
    print repr(error)
    return make_response(jsonify({'error': 'Bad request. 400'}), 400)


@app.errorhandler(404)
def not_found(error):
    print repr(error)
    return make_response(jsonify({'error': 'Not found. 404'}), 404)


@app.route('/favicon.ico')
def favicon():
    return send_from_directory(STATIC_FOLDER, 'favicon.ico', mimetype='image/x-icon')


@app.route('/static/<path:filename>')
def return_static(filename):
    return send_from_directory(STATIC_FOLDER, filename)


@app.route('/')
@app.route('/index')
def index():
    # flash(u'Hello World!', 'error')
    # return render_template("index.html")
    return send_from_directory(STATIC_FOLDER, 'index.html')


@app.route('/old')
def old_gui():
    return render_template("index.html")


@app.route('/add')
def add():
    form_add_node = AddNode()
    form_add_domain = AddDomain()
    return render_template("add.html", form_add_node=form_add_node, form_add_domain=form_add_domain)


@app.route('/api/nodes', methods=['GET'])
def get_nodes():
    nodes = Nodes.query.all()
    # serializer = NodesSchema(many=True)
    serializer = NodesSchema(many=True, only=('node_id', 'node_ip', 'node_name', 'formated_info', 'node_email'))
    result = serializer.dump(nodes)
    return jsonify({"data": result.data})


@app.route('/api/nodes/<int:node_id>', methods=['GET'])
def get_node(node_id):
    node = Nodes.query.get(node_id)
    if node is None:
        return jsonify({"message": "Node could not be found."}), 400
    return jsonify({'data': NodesSchema().dump(node).data})


@app.route('/api/nodes/<int:node_id>/domains', methods=['GET'])
def get_node_domains(node_id):
    domains = Domains.query.filter_by(domain_nodeid=node_id)
    if domains is None:
        return jsonify({"message": "Node could not be found."}), 400
    return jsonify({'data': DomainsSchema(many=True).dump(domains).data})


@app.route('/api/nodes/[<string:nodes_csl>]/domains', methods=['GET'])
def get_nodes_domains(nodes_csl):
    array = [x.strip() for x in nodes_csl.split(',')]
    domains = Domains.query.filter(Domains.domain_nodeid.in_(array))
    if domains is None:
        return jsonify({"message": "Node could not be found."}), 400
    return jsonify({'data': DomainsSchema(many=True).dump(domains).data})


@app.route('/api/nodes', methods=['POST'])
def create_node():
    form_add_node = AddNode(csrf_enabled=False)
    if not form_add_node.validate():
        return jsonify({'result': form_add_node.errors})
    try:
        new_node = Nodes(
            node_ip=form_add_node.node_ip.data,
            node_name=form_add_node.node_name.data,
            node_passwd=form_add_node.node_passwd.data,
            node_user='root',
            node_email=form_add_node.node_email.data,
            node_note=form_add_node.node_comment.data
        )
        db.session.add(new_node)
        db.session.commit()
    except IntegrityError:
        return jsonify({"message": "IntegrityError. Bad request"}), 400
    db.session.refresh(new_node)
    return jsonify({'data': NodesSchema().dump(new_node).data}), 201


@app.route('/api/nodes/<int:node_id>', methods=['PUT'])
def update_node(node_id):
    return jsonify({'result': node_id})


@app.route('/api/nodes/<int:node_id>', methods=['DELETE'])
def delete_node(node_id):
    node = Nodes.query.get(node_id)
    if node is None:
        return jsonify({"message": "Node could not be found."}), 400
    try:
        db.session.delete(node)
        db.session.commit()
    except Exception as error:
        print('caught this error: ' + repr(error))
        return jsonify({'result': False, "debug": repr(error)})
    return jsonify({'result': True})


@app.route('/run/playbook_old', methods=['POST'])
def runplaybook():
    print request.json
    if not request.json or 'playbook' not in request.json or 'args' not in request.json:
        return jsonify({'result': "Bad json request"})
    if 'node_id' not in request.json['args']:
        return jsonify({'result': "Where is node_id ?"})
    node = Nodes.query.get(request.json['args']['node_id'])
    if node is None:
        return jsonify({"message": "Node could not be found."}), 400
    log_id = Storage().flush_log_to_database("Started thread with args: %s " % request.json)
    jsonargs = NodesSchema().dump(node).data
    if 'domain_id' in request.json['args']:
        domain = Domains.query.get(request.json['args']['domain_id'])
        if domain is None:
            return jsonify({"message": "Domain could not be found."}), 400
        json_domain = DomainsSchema().dump(domain).data
        for key in json_domain:
            jsonargs[key] = json_domain[key]
    """ for debug only  """
    for key in request.json['args']:
        jsonargs[key] = request.json['args'][key]
    """ end debug block """
    jsonargs['log_id'] = log_id
    playbook_path = "playbooks/%s" % request.json['playbook']
    BFR.queue.put((playbook_path, jsonargs))
    return jsonify({'result': "OK", 'id': log_id})


def validated_json(in_json):
    # check 'playbook' and 'args' fields
    if 'playbook' not in in_json or 'args' not in in_json:
        return False
    # check 'node_id' in 'args' or 'domain_id' in 'args'
    if 'node_id' in in_json['args'] or 'domain_id' in in_json['args']:
        return True
    else:
        return False


@app.route('/run/playbook', methods=['POST'])
@app.route('/run/playbook_v2', methods=['POST'])
def runplaybook2():
    if not validated_json(request.json):
        return jsonify({'result': "Bad json request"})
    jsonargs = {}
    """ set up globals, like [nodes:vars] in inventory file """
    for key in request.json['args']:
        jsonargs[key] = request.json['args'][key]
    """ end """
    jsonargs['nodes'] = []
    jsonarg = {}
    if 'domain_id' in request.json['args'] and request.json['args']['domain_id'] != []:
        print "########################## domain_id"
        domain_id = request.json['args']['domain_id']
        if isinstance(domain_id, int) or isinstance(domain_id, unicode):
            print "########################## single domain_id"
            print domain_id
            domain_mod = Domains.query.get(domain_id)
            if domain_mod is None:
                return jsonify({"message": "domain could not be found."}), 400
            json_d = DomainsSchema().dump(domain_mod).data
            node_mod = Nodes.query.get(domain_mod.domain_nodeid)
            json_n = NodesSchema().dump(node_mod).data
            jsonarg = dict(json_d, **json_n)
            jsonargs['nodes'].append(jsonarg)
        elif isinstance(domain_id, list):
            print "########################## list domain_id"
            print domain_id
            for id in domain_id:
                domain_mod = Domains.query.get(id)
                if domain_mod is None:
                    pass
                else:
                    json_d = DomainsSchema().dump(domain_mod).data
                    node_mod = Nodes.query.get(domain_mod.domain_nodeid)
                    json_n = NodesSchema().dump(node_mod).data
                    jsonarg = dict(json_d, **json_n)
                    jsonargs['nodes'].append(jsonarg)
        else:
            return jsonify({'result': "Bad type domain_id: %s" % type(domain_id)})
    else:
        node_id = request.json['args']['node_id']
        if isinstance(node_id, int) or isinstance(node_id, unicode):
            node_mod = Nodes.query.get(node_id)
            if node_mod is None:
                return jsonify({"message": "Node could not be found."}), 400
            jsonarg = NodesSchema().dump(node_mod).data
            jsonargs['nodes'].append(jsonarg)
        elif isinstance(node_id, list):
            for id in node_id:
                node_mod = Nodes.query.get(id)
                if node_mod is None:
                    pass
                else:
                    jsonarg = NodesSchema().dump(node_mod).data
                    jsonargs['nodes'].append(jsonarg)
        else:
            return jsonify({'result': "Bad type node_id: %s" % type(node_id)})
    log_id = Storage().flush_log_to_database("Started thread with args: %s " % request.json)
    jsonargs['log_id'] = log_id
    """ solve uniq calback emails """
    s = set(dic["node_email"] for dic in jsonargs['nodes'])
    jsonargs['callback_emails'] = ', '.join(str(e) for e in s)
    """ end """
    playbook_path = "playbooks/%s" % request.json['playbook']
    BFR.queue.put((playbook_path, jsonargs))
    qsize = BFR.queue.qsize()
    return jsonify({'result': "OK", 'id': log_id, 'qsize': qsize})


@app.route('/run/playbook/progress/<int:log_id>', methods=['GET'])
def runplaybook_progress_id(log_id):
    query = db.session.query(Logs.log_type).filter_by(log_initid=log_id).all()
    log_count = len(query)
    log_type = ""
    for item in query:
        log_type += item.log_type
        log_type += " "
    # log_type  = query[0].log_type
    return jsonify({'result': "OK", 'log_id': log_id, 'status': log_type, 'count': log_count})


@app.route('/api/domains', methods=['POST'])
def create_domains():
    form_add_domain = AddDomain(csrf_enabled=False)
    if not form_add_domain.validate():
        return jsonify({'result': form_add_domain.errors})
    note = ''
    name = form_add_domain.name.data
    dbpass = pwgen()
    username = pwgen(3, 'abcdefghijklmnopqrstuvwxyz') + name.replace(".", "").replace("-", "").lower()[:13]
    if form_add_domain.node_id.data is None:
        return jsonify({"message": "Node could not be found."}), 400
    else:
        node_id = int(form_add_domain.node_id.data)
    note = form_add_domain.comment.data
    node_mod = Nodes.query.get(node_id)
    if node_mod is None:
        return jsonify({"message": "Node could not be found."}), 400
    else:
        ip = Nodes.query.get(node_id).node_ip
    passwd = pwgen()
    try:
        new_domain = Domains(
            domain_name=name,
            domain_email=form_add_domain.email.data,
            domain_note=note,
            domain_nodeid=node_id,
            domain_dbpass=dbpass,
            domain_dbuser=username,
            domain_dbname=username,
            domain_user=username,
            domain_ip=ip,
            domain_passwd=passwd,
            domain_core=form_add_domain.core.data
        )
        db.session.add(new_domain)
        db.session.commit()
    except IntegrityError:
        return jsonify({"message": "IntegrityError. Bad request"}), 400
    db.session.refresh(new_domain)
    return jsonify({'data': DomainsSchema().dump(new_domain).data}), 201


@app.route('/api/domains', methods=['GET'])
def get_domains():
    domains = Domains.query.all()
    # serializer = DomainsSchema(many=True)
    serializer = DomainsSchema(many=True,
                               only=('domain_id', 'domain_ip', 'domain_name', 'formated_info', 'domain_email'))
    result = serializer.dump(domains)
    return jsonify({"data": result.data})


@app.route('/api/domains/<int:domain_id>', methods=['GET'])
def get_domain(domain_id):
    domain = Domains.query.get(domain_id)
    if domain is None:
        return jsonify({"message": "Domain could not be found."}), 400
    return jsonify({'data': DomainsSchema().dump(domain).data})


@app.route('/api/domains/<int:domain_id>', methods=['PUT'])
def update_domain(domain_id):
    return jsonify({'result': domain_id})


@app.route('/api/domains/<int:domain_id>', methods=['DELETE'])
def delete_domain(domain_id):
    domain = Domains.query.get(domain_id)
    if domain is None:
        return jsonify({"message": "Domain could not be found."}), 400
    db.session.delete(domain)
    db.session.commit()
    return jsonify({'result': True})
