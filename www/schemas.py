#!/usr/bin/python
from marshmallow import Schema, fields


class NodesSchema(Schema):
    formated_info = fields.Method("format_info")

    @staticmethod
    def format_info(custom):
        return "{}, {}, {}, {}".format(custom.node_note, custom.node_role, custom.node_crtd, custom.node_uptd)

    class Meta:
        fields = ('node_id', 'node_name', 'node_ip', 'node_user', 'node_passwd', "formated_info", "node_email")


class DomainsSchema(Schema):
    formated_info = fields.Method("format_info")

    @staticmethod
    def format_info(custom):
        return "{}, {}, {}, {}, {}".format(custom.domain_core, custom.domain_note, custom.domain_hiden,
                                           custom.domain_crtd, custom.domain_uptd)

    class Meta:
        fields = ('domain_id', 'domain_name', 'domain_nodeid', 'domain_dbpass', 'domain_dbname',
                  'domain_dbuser', 'domain_ip', 'domain_user', 'domain_passwd', 'domain_email',
                  'domain_core', 'formated_info')
