#!/usr/bin/python
from flask_wtf import Form, RecaptchaField
from wtforms import BooleanField, StringField, FileField, SubmitField, validators, DecimalField
from wtforms.validators import DataRequired
from decimal import ROUND_DOWN


class LoginForm(Form):
    openid = StringField('openid', validators=[DataRequired()])
    remember_me = BooleanField('remember_me', default=False)


class UploadImgForm(Form):
    reqfile = FileField()
    recaptcha = RecaptchaField()
    submit = SubmitField('Submit')


class AddNode(Form):
    node_ip = StringField('IP', [validators.Length(min=7, max=15)])
    node_passwd = StringField('Password', [validators.Length(min=7, max=25)])
    node_name = StringField('Hostname', [validators.Length(min=7, max=50)])
    node_email = StringField('Email', [validators.Length(min=7, max=25)])
    node_comment = StringField('Notes', [validators.Length(min=0, max=50)])
    submit = SubmitField('Submit')


class AddDomain(Form):
    name = StringField('Name:', [validators.Length(min=4, max=50)])
    email = StringField('Email:', [validators.Length(min=6, max=25)])
    node_id = DecimalField('Node_id:', 0, ROUND_DOWN, [validators.required()])
    core = StringField('Site_CMS:', [validators.Length(min=0, max=50)])
    comment = StringField('Comment:', [validators.Length(min=0, max=50)])
    submit = SubmitField('Submit')
