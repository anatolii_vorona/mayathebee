#!/usr/bin/python
import os

CSRF_ENABLED = True
SECRET_KEY = '7632er3F#$R#e4f3rf343$#FER#F$13%&*'
BASEDIR = os.path.abspath(os.path.dirname(__file__))
UPLOAD_FOLDER = os.path.join(BASEDIR, 'content/')
STATIC_FOLDER = os.path.join(BASEDIR, 'static/')

# SQLALCHEMY_DATABASE_URI = 'sqlite:///' + os.path.join(BASEDIR, 'app.db')
# SQLALCHEMY_DATABASE_URI = 'mysql://root:@localhost/test'
SQLALCHEMY_DATABASE_URI = 'mysql://maya:zj5KjBcMbQIEP9KH@localhost/maya'

SQLALCHEMY_TRACK_MODIFICATIONS = False
SQLALCHEMY_ECHO = True
SQLALCHEMY_RECORD_QUERIES = True
SQLALCHEMY_POOL_RECYCLE = 50
SQLALCHEMY_POOL_TIMEOUT = 20
SQLALCHEMY_POOL_SIZE = 10

# I did some investigation and discovered that 
# I also had to change the SQLALCHEMY_POOL_RECYCLE to be less than the MySQL interactive_timeout. 
# On the godaddy server, interactive_timeout was set to 60, so I set SQLALCHEMY_POOL_RECYCLE to 50. 
# I think both the pattern I used, and this timeout were necessary to make the problem go away,
# but at this point I'm not positive. 
# However, I'm pretty sure that when SQLALCHEMY_POOL_RECYCLE was greater than interactive_timeout,
# I was still getting the operational error.
# interactive_timeout = 28800 # 8h default for mariadb

# http://blog.amedama.jp/entry/2015/08/15/133322
