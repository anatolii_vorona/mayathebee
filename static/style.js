var MayaApp = angular.module('MayaApp', []);
var selectedRowsIndexesNodes = [];
var selectedRowsIndexesDomains = [];
//var api_domain = 'https://maya.staff.pub-dns.org';
var api_domain = '';

MayaApp.controller('MainCtrl', function($scope, $http, sharedData) {
  var data = [];
  data = sharedData.getData();
  $scope.data = data;
  var cleanup = $scope.$on('changeValue', function() {
    console.log("from MainCtrl changeValue");
    $scope.data = sharedData.getData();
  })
  $scope.$on('$destroy', cleanup);
  $scope.playbooks = {
    yml: null,
    opts: null,
    sub_opts: null,
    availableYml: [{
      id: 'virtualmin.yml',
      name: 'Setup node'
    }, {
      id: 'run_cmd.yml',
      name: 'Run command'
    }, {
      id: 'add_shop_ceopack.yml',
      name: 'Install WP-seopack'
    }, {
      id: 'add_shop.yml',
      name: 'Install CMS'
    }, {
      id: 'del_shop.yml',
      name: 'Uninstall CMS'
    }, {
      id: 'getprofile.yml',
      name: 'Show credentials'
    }, {
      id: 'fake.yml',
      name: 'Fake yml'
    }],

    availableOptions: {
      'virtualmin.yml': [{ id: 'cpanel_function', name: 'Function' }],
      'add_shop.yml': [{ id: 'ceopack', name: 'Enable CEO-pack' }],
      'run_cmd.yml': [{ id: 'run_cmd', name: 'run_cmd' }],
      'fake.yml': [{ id: 'test', name: 'test' }]
    },

    availableSubOptions: {
      'cpanel_function': [{ id: 'managed' }, { id: 'unmanaged' }, { id: 'vps' }, { id: 'internal' }],
      'ceopack': [{ id: '0' }, { id: '1' }, { id: '2' }],
      'run_cmd': [{ id: 'update_wpapiphp' }, { id: 'reboot' }, { id: 'status' },{ id: 'update_doll' }],
      'test': [{ id: 'test0' }, { id: 'test1' }, { id: 'test2' }]
    }
  };
  $scope.submit = function() {
    json_args = {};
    buildPostData();
    runPlaybook($scope.playbooks.yml, json_args);
  };
  function buildPostData() {

    if (selectedRowsIndexesDomains!==null && selectedRowsIndexesDomains.length > 0) {
        json_args['domain_id'] = selectedRowsIndexesDomains;
    } else if (selectedRowsIndexesNodes!==null && selectedRowsIndexesNodes.length > 0) {
        json_args['node_id'] = selectedRowsIndexesNodes;
    }

    if ($scope.playbooks.sub_opts!==null && $scope.playbooks.sub_opts.length > 0) {
        json_args[$scope.playbooks.opts] = $scope.playbooks.sub_opts;
    }
  };
  function runPlaybook(pb_name, json_args) {
    console.log(JSON.stringify({
      playbook: pb_name,
      args: json_args
    }));
    $http({
      url: api_domain + '/run/playbook_v2',
      method: "POST",
      data: JSON.stringify({
        playbook: pb_name,
        args: json_args
      }),
      headers: {
        'Content-Type': 'application/json'
      }
    }).success(function(data, status, headers, config) {
      console.log(data);
    }).error(function(data, status, headers, config) {
      $scope.status = status + ' ' + headers;
      console.log($scope.status);
    });
  };
});

MayaApp.controller('NodesListCtrl', function($scope, $http, $rootScope, sharedData) {
  $http.get(api_domain + '/api/nodes').success(function(data) {
    $scope.nodes = data;
  });
  $scope.orderProp = 'node_id';
  $scope.selectRow = function(event, rowIndex) {
    if (event.ctrlKey) {
      changeSelectionStatus(rowIndex);
    } else if (event.shiftKey) {
      selectWithShift(rowIndex);
    } else {
      selectedRowsIndexesNodes = [rowIndex];
    }
    console.log(selectedRowsIndexesNodes);
    $scope.selectedRowsIndexesNodes = selectedRowsIndexesNodes;
    sharedData.setData(selectedRowsIndexesNodes);
    $rootScope.$broadcast('changeValue');
  };

  function selectWithShift(rowIndex) {
    var lastSelectedRowIndexInSelectedRowsList = selectedRowsIndexesNodes.length - 1;
    var lastSelectedRowIndex = selectedRowsIndexesNodes[lastSelectedRowIndexInSelectedRowsList];
    var selectFromIndex = Math.min(rowIndex, lastSelectedRowIndex);
    var selectToIndex = Math.max(rowIndex, lastSelectedRowIndex);
    selectRows(selectFromIndex, selectToIndex);
  }

  function selectRows(selectFromIndex, selectToIndex) {
    for (var rowToSelect = selectFromIndex; rowToSelect <= selectToIndex; rowToSelect++) {
      select(rowToSelect);
    }
  }

  function changeSelectionStatus(rowIndex) {
    if ($scope.isRowSelected(rowIndex)) {
      unselect(rowIndex);
    } else {
      select(rowIndex);
    }
  }

  function select(rowIndex) {
    if (!$scope.isRowSelected(rowIndex)) {
      selectedRowsIndexesNodes.push(rowIndex)
    }
  }

  function unselect(rowIndex) {
    var rowIndexInSelectedRowsList = selectedRowsIndexesNodes.indexOf(rowIndex);
    var unselectOnlyOneRow = 1;
    selectedRowsIndexesNodes.splice(rowIndexInSelectedRowsList, unselectOnlyOneRow);
  }

  $scope.isRowSelected = function(rowIndex) {
    return selectedRowsIndexesNodes.indexOf(rowIndex) > -1;
  };

});

MayaApp.controller('DomainsListCtrl', function($scope, $http, sharedData) {
  var selectedNodes = [];
  selectedNodes = sharedData.getData();
  $scope.selectedNodes = selectedNodes;
  var cleanup = $scope.$on('changeValue', function() {
    $scope.selectedNodes = sharedData.getData();
    rest_url = api_domain + '/api/nodes/[' + $scope.selectedNodes + ']/domains'
    console.log('from DomainsListCtrl get:' + rest_url);
    $http.get(rest_url).success(function(data) {
      $scope.domains = data;
    });
  })

  $scope.$on('$destroy', cleanup);

  $scope.orderProp = 'domain_id';
  $scope.selectRow = function(event, rowIndex) {
    if (event.ctrlKey) {
      changeSelectionStatus(rowIndex);
    } else if (event.shiftKey) {
      selectWithShift(rowIndex);
    } else {
      selectedRowsIndexesDomains = [rowIndex];
    }
    console.log(selectedRowsIndexesDomains);
    $scope.selectedRowsIndexesDomains = selectedRowsIndexesDomains;
  };

  function selectWithShift(rowIndex) {
    var lastSelectedRowIndexInSelectedRowsList = selectedRowsIndexesDomains.length - 1;
    var lastSelectedRowIndex = selectedRowsIndexesDomains[lastSelectedRowIndexInSelectedRowsList];
    var selectFromIndex = Math.min(rowIndex, lastSelectedRowIndex);
    var selectToIndex = Math.max(rowIndex, lastSelectedRowIndex);
    selectRows(selectFromIndex, selectToIndex);
  }

  function selectRows(selectFromIndex, selectToIndex) {
    for (var rowToSelect = selectFromIndex; rowToSelect <= selectToIndex; rowToSelect++) {
      select(rowToSelect);
    }
  }

  function changeSelectionStatus(rowIndex) {
    if ($scope.isRowSelected(rowIndex)) {
      unselect(rowIndex);
    } else {
      select(rowIndex);
    }
  }

  function select(rowIndex) {
    if (!$scope.isRowSelected(rowIndex)) {
      selectedRowsIndexesDomains.push(rowIndex)
    }
  }

  function unselect(rowIndex) {
    var rowIndexInSelectedRowsList = selectedRowsIndexesDomains.indexOf(rowIndex);
    var unselectOnlyOneRow = 1;
    selectedRowsIndexesDomains.splice(rowIndexInSelectedRowsList, unselectOnlyOneRow);
  }

  $scope.isRowSelected = function(rowIndex) {
    return selectedRowsIndexesDomains.indexOf(rowIndex) > -1;
  };
});

MayaApp.service('sharedData', function() {
  var data = [];
  return {
    setData: function(selectedRowsIndexes) {
      data = selectedRowsIndexes;
    },
    getData: function() {
      return data;
    }
  }
});
